import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    ScrollView,
    View,
    Alert
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import styles from './style';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Actions } from 'react-native-router-flux';

export default class App extends Component {
    state = {
        zoom: 0,
        depth: 0,
        type: 'back',
        whiteBalance: 'auto',
    };
    
    toggleFacing() {
        this.setState({
            type: this.state.type === 'back' ? 'front' : 'back',
        });
    }
    zoomOut() {
        this.setState({
            zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
        });
    }

    zoomIn() {
        this.setState({
            zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
        });
    }


    render() {
        const advanceCamera = () => {
            Actions.AdvanceMode()
          }
        return (
            <RNCamera
                ref={ref => {
                    this.camera = ref;
                }}
                style={styles.preview}
                type={this.state.type}
                zoom={this.state.zoom}
                flashMode={RNCamera.Constants.FlashMode.on}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We need your permission to use your camera phone'}
            >
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', }}>
                    <TouchableOpacity
                        onPress={this.takePicture.bind(this)}
                        style={styles.flipButton}>
                        <View style={{ padding: 5 }}>
                            <Icon name="camera" size={30} color="#fff" />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.flipButton} onPress={this.toggleFacing.bind(this)}>
                        <View style={{ padding: 5 }}>
                            <Icon name="camera-switch" size={30} color="#fff" />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
                        onPress={this.zoomIn.bind(this)}
                    >
                        <View style={{ padding: 5 }}>
                            <Icon name="plus-circle" size={30} color="#fff" />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
                        onPress={this.zoomOut.bind(this)}
                    >
                        <View style={{ padding: 5 }}>
                            <Icon name="minus-circle" size={30} color="#fff" />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
                        onPress={() => advanceCamera()}
                    >
                        <View style={{ padding: 5 }}>
                            <Icon name="camera-enhance" size={30} color="#fff" />
                        </View>
                    </TouchableOpacity>
                </View>
            </RNCamera>
        );
    }

    takePicture = async function () {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options)
           alert("Foto Guarda en "+ data.uri);
            console.log(data.uri);
        }
    };
}


