import { responsive } from '../../utils/helper';

const styles = responsive({
  containerActivity: {
    position: 'absolute',
    right: '50%',
    top: '50%'
  },
  loading: {
    width: '100%',
    height: '100%',
  }
});

export default styles;