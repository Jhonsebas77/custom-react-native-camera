import React, { Component } from 'react';
import { StyleSheet, ImageBackground, View, ActivityIndicator } from 'react-native';
import styles from './style';

export default class Loading extends Component {
  render() {
    return (
      <View style={{ alignItems: 'center' }}>
        <ImageBackground source={this.props.imageLoading} style={styles.loading}>
          <View style={styles.containerActivity}>
            <ActivityIndicator size="large" color='white' />
          </View>
        </ImageBackground>
      </View>
    );
  }
}
