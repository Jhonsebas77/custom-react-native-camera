import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import Home from '../screen/Home';
import AdvanceMode from '../screen/AdvanceMode';

export default class routes extends Component {
  render() {
    return (
      <Router navigationBarStyle={{ backgroundColor: '#81b71a' }}>
        <Scene key="root">
          <Scene key="Home" component={Home} title='Test Camera - Basic' />
          <Scene key="AdvanceMode" component={AdvanceMode} title='Test Camera - Advance' />
        </Scene>
      </Router>
    );
  }
}