import { Dimensions, StyleSheet } from "react-native";

const propsWidth = ['width', 'marginHorizontal', 'maxLength', 'marginLeft', 'marginRight', 'paddingHorizontal', 'paddingLeft', 'paddingRight', 'borderWidth', 'borderTopWidth', 'borderLeftWidth', 'borderRightWidth', 'borderBottomWidth', 'left', 'right', 'maxWidth', 'minWidth'];
const propsHeight = ['height', 'marginVertical', 'marginTop', 'marginBottom', 'paddingVertical', 'paddingTop', 'paddingBottom', 'borderHeight', 'borderTopHeight', 'borderLeftHeight', 'borderRightHeight', 'borderBottomHeight', 'fontSize', 'bottom', 'top', 'maxHeight', 'minHeight'];
const propsGeneral = ['margin', 'padding', 'borderRadius', 'borderBottomLeftRadius', 'borderBottomRightRadius', 'borderTopLeftRadius', 'borderTopRightRadius'];
const { height, width } = Dimensions.get("window");
const Sugar = require('sugar/number');
const baseWidth = 360;
const baseHeight = 640;

const wFactor = Sugar.Number.round((width / baseWidth), 2)
const hFactor = Sugar.Number.round((height / baseHeight), 2)

export const responsive = (style) => {
    Object.keys(style).map(key => {
        for (const prop in style[key]) {
            if (propsGeneral.includes(prop) || propsWidth.includes(prop)) {
                style[key][prop] = style[key][prop] * wFactor;
            } else {
                if (propsHeight.includes(prop)) {
                    style[key][prop] = style[key][prop] * hFactor;
                }
            }
        }
    });

    return StyleSheet.create(style);
}
