﻿# custom-react-native-camera
custom-react-native-camera es un ejemplo de la libreria <kbd>react-native-camera</kbd> utilizando principalmente <kbd>RNCamera</kbd> y  <kbd>FaceDetector</kbd>

- https://github.com/react-native-community/react-native-camera

- https://github.com/react-native-community/react-native-camera/blob/master/docs/RNCamera.md


*Desarrollado por:* **Sebastian Otalora**

## Instalación
Primero se debe clonar el proyecto
<kbd>**SSH:** git@gitlab.com:Jhonsebas77/custom-react-native-camera.git</kbd>

## Configuración Proyecto
 Una vez clonado el repositorio, configuremos lo básico para poder ejecutar el proyecto  
 
   -  Seguimos los siguientes pasos
	     -  1\. En la terminal nos ubicamos en el directorio con el proyecto en *React-Native* con <kbd>cd /nombre_proyecto </kbd>  
	     - 2\. Ejecutamos <kbd>npm install </kbd>  
	     **Nota:** Si no encuentra las dependencias instalar:
		   - <kbd>react-native-camera @ 1.0.3 </kbd>  
		   - <kbd>react-native-vector-icons @ 4.6.0 </kbd>  
		   - <kbd> react-native-router-flux @ 4.0.0-beta.28</kbd>  
	   - 3\. En  terminales distintas ejecutamos <kbd>react-native start </kbd> en la primera ventana, una vez indique *"Loading dependency graph, done."* ejecutamos en la segunda ventana <kbd>react-native run-android</kbd> o <kbd>react-native run-ios</kbd> dependiendo que necesitemos

## Capturas Pantalla

#### Camara Basica
![ ](https://lh3.googleusercontent.com/IZg4OK-NcuvvCY_fRQNJssW2iNZ4tLfqoVCSX2ekP0JaQzVhgDQ-k7m3khJHUsv89IQTxnE8M-41NKjm805ZLenNMSUsGx9OUWxuPZaK66rrImG8F-5Vva7nd32rJMQFIhmF28Bk2FB0XZJnX9s60N8ZtzYyN33Svvj5fPlEAkQGRV7mC3EIfnGIRKChSx6rk3_XkZOHc-PFmU0UE60fGoGVseKOtLybv_992N9YhO3AbRRyElJyAaDs4KcPwUVQtvjVZhoqLq12qt_aYggrrMPjvTlgpWQPXqQmg7J-Af4vN98WF3cqV7tWPOuERF6YzTwb4TlQ9OpM-C_QSfU9xNipmV-tova-e8gjPzSz-TjsXQcKBlg1LKgMWqpI-JXZgFl8RRkLAsVDq1GN_QM30wT3JsRuOSV14mD_zfV-rIe0snqMPACSF5_KTP15I1_fpkVob4HOV6ouKS8xY27-FNGzgjXnXWvDn1wnPvkzhtZROIod58R4b2Mn93VkigCORdFegeSFvS53fqD_QYgBi2YmwcPvysXKefKQ6GWSZJK8hI7HyLbcTzYpp_BSCXEDYZ9VqBYshbi39k0i-7nfdQCvoHr7AbGHHNDJ_1K9=w1004-h1606-no)

> - Screen que visualiza la camara y posee funcionalidades básicas de la camara

#### Camara Avanzada
![ ](https://lh3.googleusercontent.com/XSA2a5ASfqLXXUQwnDq8lo7DOJ9gn-OmXNEKZl6036k0RTgQNc_yOCe-fRdfpsG2N8F5syCRl7cmVEosfuZ9LHbaWRLymAFV9JiWQRofO3ONZYV4WfS8_akyPhPX6H2A3e-Ysb-Eax-idmwDMxzr2dpK6e5agC0muSznvbcS0otGC6Xjrf5-3unRjOurBwZ7kstSOIoMGb2T5bRRqo8DIIsrn8O12ePo9yY636yFBuqKB6CgfC7X7xSENky0pvtwlLS4w8fckV5C80Cr4f-ZewLChH7aScA-_gFWhjwIykPXpNh0vG56wmu8PEKlnFpZUf7q8x2O0-2u9i823TZyzoAeQmyaIP45q6sgb_g74hkoyBxUBhiJJWCf4RV-luYNhe7FpXYhIbJkhsGv1G_qu8si0ELpKv4kzhFHEb-egAmbMlFDsEV04jruSE9Dwt_l-09GFT3n53DhEX0NBAENcPM9ztw3SCuVbK6OUTHOyMvxqhUj58dcI6MmECh0j_vv97NNz9AEe9uNpWTcjEAkJWjw22wdDGJj41IgfQLn0V7tP6GaSU3YRJBaml1T-9POvDFAkMSbAPCClxWS5xsa7QPCcueFWz7VzqIM8JlH=w1004-h1606-no)
> -Screen que visualiza la camara y posee funcionalidades avanzadas de la camara
## Funcionalidades
 -  Camara Basica
	   -  Zoom In
	   - Zoom Out
	   - Flip Camera
	   - Save in cache
	   - Use Flash
- Camara Avanzada   
	-  Zoom In
	  - Zoom Out
	   - Flip Camera
	   - Save in cache
	   - Use Filter
	   - Use Autofocus
	   - Use Flash
